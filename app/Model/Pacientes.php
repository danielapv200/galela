<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pacientes extends Model
{
    protected $table = "Pacientes";

    protected $primaryKey = 'IdPaciente';
}
